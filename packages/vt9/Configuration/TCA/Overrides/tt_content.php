<?php
/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\B13\Container\Tca\Registry::class)->configureContainer(
    (
    new \B13\Container\Tca\ContainerConfiguration(
        '2cols', // CType
        '2 Spalten', // label
        '50% / 50%', // description
        [
            [
                ['name' => 'Linke Spalte', 'colPos' => 201, 'disallowed' => ['CType' => '2cols,3cols,4cols']],
                ['name' => 'Rechte Spalte', 'colPos' => 202, 'disallowed' => ['CType' => '2cols,3cols,4cols']]
            ]
        ] // grid configuration
    )
    )
        ->setIcon('EXT:container/Resources/Public/Icons/container-2col.svg')
        ->setSaveAndCloseInNewContentElementWizard(false)
);
$GLOBALS['TCA']['tt_content']['types']['2cols']['showitem'] = 'sys_language_uid,CType,header,header_layout,header_position,layout,colPos,tx_container_parent';

\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\B13\Container\Tca\Registry::class)->configureContainer(
    (
    new \B13\Container\Tca\ContainerConfiguration(
        '3cols', // CType
        '3 Spalten', // label
        '33% / 33% / 33/', // description
        [
            [
                ['name' => 'Linke Spalte', 'colPos' => 201, 'disallowed' => ['CType' => '2cols,3cols,4cols']],
                ['name' => 'Mittlere Spalte', 'colPos' => 202, 'disallowed' => ['CType' => '2cols,3cols,4cols']],
                ['name' => 'Rechte Spalte', 'colPos' => 203, 'disallowed' => ['CType' => '2cols,3cols,4cols']]
            ]
        ] // grid configuration
    )
    )
        ->setIcon('EXT:container/Resources/Public/Icons/container-3col.svg')
        ->setSaveAndCloseInNewContentElementWizard(false)
);
$GLOBALS['TCA']['tt_content']['types']['3cols']['showitem'] = 'sys_language_uid,CType,header,header_layout,header_position,layout,colPos,tx_container_parent';

\TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\B13\Container\Tca\Registry::class)->configureContainer(
    (
    new \B13\Container\Tca\ContainerConfiguration(
        '4cols', // CType
        '4 Spalten', // label
        '4 Spalten mit je 25%', // description
        [
            [
                ['name' => 'Linke Spalte', 'colPos' => 201, 'disallowed' => ['CType' => '2cols,3cols,4cols']],
                ['name' => 'Mittlere Spalte links', 'colPos' => 202, 'disallowed' => ['CType' => '2cols,3cols,4cols']],
                ['name' => 'Mittlere Spalte rechts', 'colPos' => 203, 'disallowed' => ['CType' => '2cols,3cols,4cols']],
                ['name' => 'Rechte Spalte', 'colPos' => 204, 'disallowed' => ['CType' => '2cols,3cols,4cols']]
            ]
        ] // grid configuration
    )
    )
        ->setIcon('EXT:container/Resources/Public/Icons/container-4col.svg')
        ->setSaveAndCloseInNewContentElementWizard(false)
);
$GLOBALS['TCA']['tt_content']['types']['4cols']['showitem'] = 'sys_language_uid,CType,header,header_layout,header_position,layout,colPos,tx_container_parent';
