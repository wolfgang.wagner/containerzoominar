# EXT:container

Relevante Dateien und Verzeichnisse:
* packages/vt9/Configuration/TCA/Overrides/tt_content.php
* packages/vt9/Configuration/TypoScript/Setup/ext.container.typoscript
* packages/vt9/Resources/Private/Container/Templates/

Falls Du mit DDEV arbeitest, kannst Du dir die komplette Installation lokal einrichten:
1. ddev start
2. ddev composer install
3. ddev import-db < database.sql
4. ddev launch /typo3

Backend-Benutzername: admin
Passwort: password
